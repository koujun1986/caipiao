package com.koujun.meinv.server;

import java.io.File;
import java.io.IOException;

public class ClearTmp {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		File dir = new File("C:/Users/Jun Kou/AppData/Local");
		delete(dir);
	}

	public static void delete(File f) throws IOException {
		if (f.isDirectory()) {
			File[] listFiles = f.listFiles();
			if (listFiles != null) {
				for (File dir : listFiles) {
					delete(dir);
				}
			}
		}
		System.out
				.println("[DELETE]" + f.getAbsolutePath() + "/" + f.getName());
		f.delete();
	}

}
