package com.koujun.meinv.server.caipiao;

import java.io.IOException;

import com.koujun.meinv.server.StringUtils;

public class BitFinder {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.getProperties().setProperty("http.proxyHost", "127.0.0.1");
		System.getProperties().setProperty("http.proxyPort", "8580");
		
		String res = StringUtils.requestURLAsString("http://thepiratebay.se/browse/500/29/3");
		int start = res.indexOf("<table id=\"searchResult\">");
		int end = res.indexOf("</table>", start);
		res = res.substring(start, end);
		String[] split = res.split("</tr>");
		for (int i = 1; i < split.length; i++) {
			int _s = split[i].trim().indexOf("<div class=\"detName\">");
			int _e = split[i].trim().indexOf("</a>", _s);
			if (_s < 0 || _e < 0) {
				continue;
			}
			String substring = split[i].trim().substring(_s, _e).replace("<div class=\"detName\">", "").replace("<a href=\"", "http://thepiratebay.se/").trim();
			String title  = substring.substring(substring.lastIndexOf(">")+1);
			System.out.println(substring+"\t"+title);
		}

	}
}
