package com.koujun.meinv.server;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

public class MailReporter {

	private String smtpHost;
	private String username;
	private String password;
	private String from;

	private int port;

	public MailReporter() {
		this.smtpHost = "smtp.163.com";
		this.username = "meijutool";
		this.password = "19860824";
		this.port = 25;
		this.from = "meijutool@163.com";
	}

	private Email get() throws EmailException {
		Email email = new SimpleEmail();
		email.setHostName(smtpHost);
		email.setSmtpPort(port);
		email.setAuthentication(username, password);
		email.setSSL(true);
		email.setFrom(from);
		return email;
	}

	private Email getHTMLClient() throws EmailException {
		Email email = new HtmlEmail();
		email.setHostName(smtpHost);
		email.setSmtpPort(port);
		email.setAuthentication(username, password);
		email.setSSL(true);
		email.setFrom(from);
		email.setCharset("utf-8");
		return email;
	}

	public void reportException(Exception e) {

	}

	public void send(String sub, String content, String to) throws EmailException {
		get().setSubject(sub).setMsg(content).addTo(to).send();
	}

	public void sendHTML(String sub, String content, String[] to) throws EmailException {
		Email email = getHTMLClient().setSubject(sub).setMsg(content);
		for (String t : to) {
			email.addTo(t);
		}
		email.send();
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
