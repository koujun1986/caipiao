package com.koujun.meinv.server.web;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.PropertyConfigurator;

import com.koujun.meinv.server.caipiao.CaipiaoChecker;

public class StartFilter implements Filter {

	@Override
	public void destroy() {
	}



	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException,
			ServletException {
		arg2.doFilter(arg0, arg1);
	}



	@Override
	public void init(FilterConfig arg0) throws ServletException {
		Properties prop = new Properties();
		prop.setProperty("log4j.rootLogger", "INFO, A1");
		prop.setProperty("log4j.appender.A1", "org.apache.log4j.ConsoleAppender");
		prop.setProperty("log4j.appender.A1.layout", "org.apache.log4j.PatternLayout");
		prop.setProperty("log4j.appender.A1.layout.ConversionPattern", "%d [%t] %-5p %c - %m%n");
		PropertyConfigurator.configure(prop);
		Thread thread3 = new Thread(new CaipiaoChecker());
		thread3.setName("CaipiaoChecker");
		thread3.start();
	}

}
