package com.koujun.meinv.server.caipiao;

import java.util.ArrayList;
import java.util.List;

public class Item {
	public String name = null;
	public String value = null;

	public Item(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public static List<Item> getFormParam(String html) {
		String[] split = html.split("<input type=\"hidden\"");
		String item = null;

		List<Item> list = new ArrayList<Item>();
		for (int i = 1; i < split.length; i++) {
			item = split[i];
			list.add(parseItem(item));
		}
		return list;
	}

	public static Item parseItem(String item) {
		int name_start = item.indexOf("name=\"");
		int name_end = item.indexOf("\" ", name_start);
		int value_start = item.indexOf("value=\"");
		int value_end = item.indexOf("\" ", value_start);
		String name = item.substring(name_start + "name=\"".length(), name_end);
		String value = item.substring(value_start + "value=\"".length(), value_end);
		return new Item(name, value);
	}
	
}