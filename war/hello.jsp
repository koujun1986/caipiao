<%@ page language="java" import="java.util.*,java.net.URL" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Hello World</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  
  <body>
  <div class="container">
	  <div class="row">
	  <div class="span12">
	   <%
	   		Properties properties = System.getProperties();
			StringBuffer buffer = null;
			for(Object key: properties.keySet()){
				if (buffer == null) {
					buffer = new StringBuffer();
	                buffer.append("<h1>System.getProperties()</h1><table class=\"table table-condensed table-striped table-bordered\">");
				} else {
					buffer.append("<tr>");
				}
				buffer.append("<td>"+key + "</td><td>" + properties.getProperty(key.toString())+"</td></tr>");
			}
		
			buffer.append("</table><h1>System.getenv()</h1><table class=\"table table-condensed table-striped table-bordered\">");
			Map<String, String> env = System.getenv();
			
			for(Object key: env.keySet()){
				buffer.append("<tr><td>"+key + "</td><td>" + env.get(key)+"</td></tr>");
			}
			buffer.append("</table>");
			
	   %>
	   <%=buffer%>
	   </div>
	   </div>
   </div>
  </body>
</html>
